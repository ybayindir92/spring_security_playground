package com.yasemin.security_playground.config;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SecurityConfigTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void contentShouldBeProtected() {
        // intentionally left blank
    }

    @Test
    public void shouldLoginWithCorrectCredentials() {
        // intentionally left blank
    }

    @Test
    public void shouldNotLoginWithWrongCredentials() {
        // intentionally left blank
    }

}