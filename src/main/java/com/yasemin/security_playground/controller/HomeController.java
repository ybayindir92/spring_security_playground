package com.yasemin.security_playground.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping(value = {"/", "/welcome"})
    public String greeting() {
        return "welcome";
    }

}
