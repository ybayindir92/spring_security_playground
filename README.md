# Spring_Security_Playground

A Spring Boot + Security playground to learn more about security framework.

The branches offer their own kind of solution 
while the main branch provides the minimal skeleton of secured web application.